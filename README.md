# Hello world 👋

Hi, my name is Thorben ([pronunciation](https://en.wiktionary.org/wiki/Thorben#German)). While I grew up in Hamburg, Germany, I am currently living in Zurich, Switzerland ([CET/CEST](https://www.timeanddate.com/worldclock/switzerland/zurich)) with my partner Viktoria.

I currently serve as the CTO of [LUUCY](https://www.luucy.ch) - a [CesiumJS](https://github.com/CesiumGS/cesium) based 3D web platform bringing together the world of high resolution geo data and high fidelity CAD/BIM models. Before I served as the CTO of [cividi](https://www.cividi.ch) - a geo spatial data consultancy for urban planning related topics. While at cividi I worked on a number of data and visualisation related projects around urban design and mobility as well as architecting open source, geodata enabled data workflows.
